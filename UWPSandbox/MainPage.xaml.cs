﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace UWPSandbox
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            CheckSwap(0,0);
        }
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            CheckSwap(0, 1);
        }
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            CheckSwap(0, 2);
        }
        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            CheckSwap(1, 0);
        }
        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            CheckSwap(1, 1);
        }
        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            CheckSwap(1, 2);
        }
        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            CheckSwap(2, 0);
        }
        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            CheckSwap(2, 1);
        }
        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            CheckSwap(2, 2);
        }
        private void Button_Click_Scramble(object sender, RoutedEventArgs e)
        {
            int rHolder; //integer for holding random values 

            Random rand = new Random();//generates random values to pull from nList

            List <String> nList = new List <String>(); //list to hold values that will be assigned to tiles

            Dictionary <int, Button> TileButtons= new Dictionary<int, Button>();
            //Load Dictionary with Keys and Buttons
            TileButtons.Add(1, Button1);
            TileButtons.Add(2, Button2);
            TileButtons.Add(3, Button3);
            TileButtons.Add(4, Button4);
            TileButtons.Add(5, Button5);
            TileButtons.Add(6, Button6);
            TileButtons.Add(7, Button7);
            TileButtons.Add(8, Button8);
            TileButtons.Add(9, Button9);
            //Fill nList with values 1-8 and an empty string
            for(int i = 1; i < 9; i++)
            {
                nList.Add(i.ToString());
            }
            nList.Add(" ");

            for(int i = 9; i > 0; i--)
            {
                rHolder = rand.Next(nList.Count);
                TileButtons[i].Content = nList[rHolder];
                nList.RemoveAt(rHolder);
            }

        }
        private void Button_Click_Solve(object sender, RoutedEventArgs e)
        {
            Button[,] BUTTONS = LoadButtonArray();
            int nullX = 3; //
            int nullY = 3;

            //logic to locate empty tile 
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (BUTTONS[i, j].Content.ToString() == " ")
                    {
                        nullX = i;
                        nullY = j;
                        BUTTONS[i, j].Content = nullX + ", " + nullY;
                    }
                }
            }
        }
        private Button[,] LoadButtonArray()
        {
            //Loads and returns a two dimensional array mirroring the button arrangement 

            Button[,] ButtonArray = new Button[3,3] { {Button1, Button2, Button3},
                                                      {Button4, Button5, Button6},
                                                      {Button7, Button8, Button9}};

            return ButtonArray;
        }
        private void CheckSwap (int x , int y)
        {
            Button[,] BUTTONS = LoadButtonArray();

            int nullX = 3; //holds X Coordinate for empty tile
            int nullY = 3; //holds Y Coordinate for empty tile

            bool PassX = false; //boolean that is used for final confirmation of valid swap on X Plane
            bool PassY = false; //boolean that is used for final confirmation of valid swap on Y Plane
            bool PassDiag = false;

            //logic to locate empty tile 
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (BUTTONS[i, j].Content.ToString() == " ")
                    {
                        nullX = i;
                        nullY = j;
                    }
                }
            }

            if (x == nullX && y == nullY)
                return;

            //Compare provided x and y against empty tile and checks if the swap is valid
            if (nullX == 0)
            {
                if (x == 1 || x == 0)
                    PassX = true;
            }
            else if (nullX == 2)
            {
                if (x == 1 || x == 2)
                    PassX = true;
            }
            else if (nullX == 1)
                PassX = true;

            if (nullY == 0)
            {
                if (y == 1 || y == 0)
                    PassY = true;
            }
            else if (nullY == 2)
            {
                if (y == 1 || y == 2)
                    PassY = true;
            }
            else if (nullY == 1)
                PassY = true;

            if (x == nullX || y == nullY)
                PassDiag = true;

            if (PassX == true && PassY == true && PassDiag == true)
            {
                BUTTONS[nullX, nullY].Content = BUTTONS[x, y].Content.ToString();
                BUTTONS[x, y].Content = " ";

            }
        }
    }
}
